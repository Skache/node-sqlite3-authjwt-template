var express = require('express');
var app = express();
const cors = require('cors');


var db =  require("./src/services/DatabaseService")
const router = require('./src/router/router');

app.use(cors());
app.use(express.json());

// set up port
const PORT = process.env.PORT || 3000;



app.use(router);
// const logger = require('./utils/logger');

var server = app.listen(PORT, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});

process.on('SIGINT', handleShutdownGracefully);
process.on('SIGTERM', handleShutdownGracefully);
process.on('SIGHUP', handleShutdownGracefully);

function handleShutdownGracefully() {
  console.log(`{"level": "info", "msg": "Closing server gracefully..."}`);
  process.exit(0); // if required
}
