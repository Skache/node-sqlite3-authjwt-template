var db = require('../services/DatabaseService');
const { v4: uuid } = require('uuid');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

function registerUser(req, res) {
  return new Promise((resolve, reject) => {
    getUsersByUsername(req.body.username).then((result) => {
      if (result.length == 0) {
        //The username is available
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          let userId = uuid();
          let user = { id: userId, username: req.body.username, password: hash, registerDate: new Date().toLocaleString() };
          insertUser(user).then((result, err) => {
            if (err) {
              resolve({message:'Error when createing the user ' + err});
            } else {
              resolve({message:'user created succesfully'});
            }
          });
        });
      } else {
        resolve({message:'THe username is not available'});
      }
    });
  });
}


function loginUser(req,res) {
  return new Promise((resolve, reject) => {
    getUsersByUsername(req.body.username).then((result) => {
      if (result.length > 0) {
        //The username exists
        // check passwor
        console.log("User")
        console.log(result[0].password)
        bcrypt.compare(req.body.password, result[0].password, (bErr, bResult) => {
            console.log(bResult)
          if (bErr || !bResult) {
            console.log("Wrong Password")
            resolve({message: 'Username or password is incorrect!'});  
          }
          if (bResult) {
            const token = jwt.sign({ username: result[0].username, userId: result[0].id }, process.env.JWT_SECRET_KEY, { expiresIn: '7d' });
            updateLoginUserByID(result[0].id).then((result) => {
              let responseObj = { msg: 'Logged in!', token, user: result[0] };
              resolve(responseObj);
            });
          }
        });
      } else {
        resolve({message: 'Username or password is incorrect!'});
      }
    });
  });
}


function hasPermission(req,res) {
  return new Promise((resolve, reject) => {
    console.log("Received permission check")
    console.log(req.headers)
    let token = req.headers.authorization.split(' ')[1];

    //TODO if the page does not exist in the databse return false
    let pageName = req.headers["pagename"]
    console.log("Pagename")
    console.log(pageName)
    if(typeof(token) =="undefined" || token =="null" ){
      resolve({hasPermission:"unlogged" })
    }else{
      const decoded = jwt.decode(token);
      console.log(decoded)
      checkUserPemission(decoded.userId,pageName).then(result => {
        if(result.length>0){
          resolve({hasPermission:true })
        }else{
          resolve({hasPermission:false })
        }
      });
    }
  });
}






//DATABSE ACCESS
function updateLoginUserByID(id) {
  return new Promise((resolve, reject) => {
    let updateUsersQuery = ' UPDATE users SET lastLogin = ? WHERE id = ?';
    db.run(updateUsersQuery, [ new Date().toLocaleString(),id])
      .then((result) => {
        resolve(result);
        return result;
      })
      .catch((err) => {
        console.log('Errordeuten ocurred when updating the login by id: ' + err);
        reject(err);
      });
  });
}

function checkUserPemission(userID,page) {
  return new Promise((resolve, reject) => {
    let getPermissionQuery = "SELECT * FROM page_role WHERE pageID = ? AND roles LIKE '%' || (SELECT role FROM users WHERE id = ?) || '%';"
    db.all(getPermissionQuery, [page,userID])
      .then((result) => {
        resolve(result);
        return result;
      })
      .catch((err) => {
        console.log('Erroe ocurred when checking permission: ' + err);
        reject(err);
      });
  });
}

function getUsersByUsername(username) {
  return new Promise((resolve, reject) => {
    let getUsersQuery = 'SELECT * FROM users WHERE LOWER(username) = LOWER( ? )';
    db.all(getUsersQuery, [username])
      .then((result) => {
        resolve(result);
        return result;
      })
      .catch((err) => {
        console.log('Erroe ocurred when getting the User by username: ' + err);
        reject(err);
      });
  });
}

function insertUser(user) {
  return new Promise((resolve, reject) => {
    let insertUsersQuery = 'INSERT INTO users (id, username, password, registerDate) VALUES ( ? , ? ,  ? , ? )';
    db.run(insertUsersQuery, [user.id, user.username, user.password, user.registerDate])
      .then((result) => {
        resolve(this.lastId);
        return result;
      })
      .catch((err) => {
        console.log('Erroe ocurred when Inserting the User: ' + err);
        reject(err);
      });
  });
}

module.exports = {
  registerUser,
  loginUser,
  hasPermission
};
