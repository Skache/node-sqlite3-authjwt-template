const express = require('express');
// const uploadController = require('./routes/uploadController');
// const downloadController = require('./routes/downloadController');
const router = express.Router();
const userController = require('../controllers/UserController');
const userValidation= require('../utils/validation/userValidation.js');
const middlewareService= require('../services/MiddlewareService');


//I will do this for every route that needs to have validation
//TODO role validation
// router.use("/",(req,res,next)=>{
//     middlewareService.isLoggedIn(req,res,()=>{
//         next();
//     })
// })

// router.get('/', (req, res, next) => {
// //Code
// });



router.post('/register', (req, res, next) => {
    userValidation.validateRegister(req,res,next);
    userController.registerUser(req,res).then(msg=>{
        res.send(msg);
    });
});



router.post('/login', (req, res, next) => {
    // userValidation.validateRegister(req,res,next);
    userController.loginUser(req,res).then(msg=>{
        res.send(msg);
    });
});

router.get('/hasPermission', (req, res, next) => {
    // userValidation.validateRegister(req,res,next);
    userController.hasPermission(req,res).then(msg=>{
        res.send(msg);
    });
});

module.exports = router;
