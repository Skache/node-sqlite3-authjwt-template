const sqlite3 = require("sqlite3").verbose();
const path = require("path");
require('dotenv').config();
const fs = require('fs');

class Database {
  constructor() {
    if (!Database.instance) {
        const rootFolder = path.dirname(require.main.filename); 
        console.log(rootFolder)
        const dbName = process.env.AUTH_DATABASE_NAME;
    
        const completeRoute = path.resolve(rootFolder, dbName);
      const db = new sqlite3.Database(completeRoute, (err) => {
        if (err) {
          console.error(err.message);
        } else {
          console.log(`Connected to the ${dbName} database.`);
        }
      });
      Database.instance = db;
    }
  }

  run(query, params) {
    return new Promise((resolve, reject) => {
      Database.instance.run(query, params, function (err) {
        if (err) {
          console.error(err.message);
          reject(err);
        } else {
          console.log(`Rows affected: ${this.changes}`);
          resolve(this.lastID);
        }
      });
    });
  }

  all(query, params ) {
    return new Promise((resolve, reject) => {
      Database.instance.all(query, params, function (err,rows) {
        if (err) {
          console.error(err);
          reject(err);
        } else {
          console.log(`Rows Returned:`);
          console.log(rows);
          resolve(rows);
        }
      });
    });
  }



}

module.exports = new Database();
